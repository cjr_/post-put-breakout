const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const poops = [];

app.get('/poop', (req, res) => {
  res.json(poops);
});

app.post('/poop', (req, res) => {
  console.log(req.body);

  const poop = {
    id: poops.length,
    name: req.body.name,
    tagline: req.body.tagline
  };

  poops.push(poop);

  res.json(poop);
});

app.put('/poop/:id', (req, res) => {
  // find the poop with the given id - req.params.id

  let foundPoop = null;

  for (var i = 0; i < poops.length; i++) {
    if(poops[i].id == req.params.id) {
      foundPoop = poops[i];
      break;
    }
  }

  if(foundPoop) {
    if(req.body.name) {
      foundPoop.name = req.body.name;
    }

    if(req.body.tagline) {
      foundPoop.tagline = req.body.tagline;
    }

    // ['name', 'tagline'].forEach(prop => {
    //   if(req.body[prop]) {
    //     foundPoop[prop] = req.body[prop];
    //   }
    // });

    res.json(foundPoop);
  } else {
    // not found
    res.status(404);
    res.json({
      message: 'Not found.'
    });
  }

});

app.listen(process.env.PORT || 3000, function() {
  console.log('Listening on port 3000');
});
