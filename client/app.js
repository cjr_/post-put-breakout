$(document).ready(function() {
  $('#addButton').click(addPoo);
  $('#updateButton').click(updatePoo);
});

const API_URL = 'http://localhost:3000/poop';

function addPoo() {
  const name = $('#name').val();
  const tagline = $('#tagline').val();

  const poop = {
    name,
    tagline
  };

  $.post(API_URL, poop, function(result) {
    console.log(result);
  });

  // const stringPoop = JSON.stringify(poop);
  //
  // console.log(stringPoop);
  //
  // $.ajax({
  //   url: API_URL,
  //   type: 'POST',
  //   dataType: 'json',
  //   contentType: 'application/json; charset=utf-8',
  //   data: stringPoop,
  //   success: function(result) {
  //     console.log('Created!', result);
  //   }
  // });
}


function updatePoo() {
  const id = $('#update_id').val();
  const name = $('#update_name').val();
  const tagline = $('#update_tagline').val();

  const poop = {
    id,
    name,
    tagline
  };

  const stringPoop = JSON.stringify(poop);

  $.ajax({
    url: API_URL + '/' + id,
    type: 'PUT',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    data: stringPoop,
    success: function(result) {
      console.log('Updated!', result);
    }
  });
}
